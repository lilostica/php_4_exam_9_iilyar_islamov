<?php

namespace App\Orchid\Screens;

use App\Comment;
use App\Photo;
use App\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create comment';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Manage comment';
    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if ($this->exists)
        {
            $this->name = 'Edit comment';
        }
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create Comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('comment.user_id')
                    ->title('Author')
                    ->fromModel(User::class,'email','id')
                    ->required(),

                Relation::make('comment.photo_id')
                    ->title('Photo')
                    ->fromModel(Photo::class,'title','id')
                    ->required(),

                Select::make('comment.assessment')->options([
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                ])->title('Assessment'),

                TextArea::make('comment.body')
                    ->title('Body')
                    ->required()
                    ->rows(10),
            ])
        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have successfully created comment.');
        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('You have successfully deleted the comment.')
            :Alert::warning('An error has occurred');
        return redirect()->route('platform.comments.list');
    }
}
