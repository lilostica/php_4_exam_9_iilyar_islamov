<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * PhotoController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $photos = Photo::paginate(9);
        return view('photo.index',compact('photos'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('photo.create');
    }


    /**
     * @param PhotoRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PhotoRequest $request)
    {
        $user = $request->user();
        $photo = new Photo();
        $photo->user_id = $user->id;
        $photo->title = $request->input('title');
        $file = $request->file('photo');
        if (!is_null($file)) {
            $path = $file->store('photo', 'public');
            $photo['photo'] = $path;
        }
        $photo->save();
        return redirect(route('photo.index'))->with('status', "Photo {$photo->title} success added!");
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id);
        return view('photo.show',compact('photo'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $photo = Photo::findOrFail($id);
        $this->authorize('edit',$photo);
        return view('photo.edit',compact('photo'));
    }


    /**
     * @param PhotoRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PhotoRequest $request, $id)
    {
        $photo = Photo::findOrFail($id);
        $data = $request->all();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $path = $file->store('photo', 'public');
            $data['photo'] = $path;
        }
        $photo->update($data);
        return redirect(route('photo.index'))->with('status', "Photo {$photo->title} success updated!");
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $photo = Photo::findOrfail($id);
        $this->authorize('delete',$photo);
        $photo->delete();
        return back()->with('status', "Photo {$photo->title} success deleted!");
    }
}
