<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentReguest;
use App\Photo;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    /**
     * CommentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param CommentReguest $request
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CommentReguest $request)
    {
        $user = $request->user();
        $comment = new Comment();
        $comment->user_id = $user->id;
        $comment->photo_id = $request->input('photo_id');
        $comment->body = $request->input('body');
        $comment->assessment = $request->input('assessment');
        $comment->save();
        return back()->with('status', 'Comment has added');
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {

        $comment = Comment::findOrFail($id);
        $this->authorize('edit',$comment);
        return view('comment.edit',compact('comment'));
    }


    /**
     * @param CommentReguest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CommentReguest $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $photo_id = $request->input('photo_id');
        $user = $request->user();
        $comment->user_id = $user->id;
        $comment->photo_id = $photo_id;
        $comment->body = $request->input('body');
        $comment->assessment = $request->input('assessment');
        $comment->save();
        return redirect(route('photo.show',$photo_id))->with('status', 'Comment has updated');

    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('delete',$comment);
        $comment->delete();
        return back()->with('status', 'Comment has deleted');
    }
}
