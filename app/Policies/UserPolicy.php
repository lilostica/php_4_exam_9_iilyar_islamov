<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function display(User $user)
    {
        $auth = Auth::id();
        return $user->id === $auth;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function displayCreateButton(User $user,User $photo_user_id)
    {
        return $user->id === $photo_user_id->id;
    }
}
