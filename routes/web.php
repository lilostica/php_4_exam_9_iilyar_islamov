<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware'=>'language'],function ()
{
    Route::get('/', 'PhotoController@index');

    Route::resource('photo', 'PhotoController');
    Route::get('user/{id}','UserController@show')->name('user-show');
    Route::resource('comment', 'CommentController')->except(['index','show','create']);

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
});

Route::get('language/{locale}', 'LanguageSwitcherController@switcher')
    ->name('language.switcher')
    ->where('locale', 'en|ru');
