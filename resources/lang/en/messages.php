<?php
return [
       'nav_brand' => 'Photo Gallery',
       'all_photo' => 'All Photo',
       'author' => 'Author',
       'avg' => 'Assessment avg',
       'comments' => 'Comments',
       'enter_comment' => 'Enter Comment',
       'assessment' => 'Assessment',
       'add_comment' => 'Add Comment',
       'delete' => 'Delete',
       'edit' => 'Edit',
       'update_photo' => 'Update Photo',
       'photo_title' => 'Photo title',
       'chose_picture' => 'Chose picture',
       'add_photo' => 'Add photo',
       'update_comment' => 'Update Comment',
       'my_page' => 'My Page',

    ];
