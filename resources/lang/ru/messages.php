<?php
return [
    'nav_brand' => 'Фото Галерея',
    'all_photo' => 'Все Фото',
    'author' => 'Автор',
    'avg' => 'Среднея оценка',
    'comments' => 'Коментарии',
    'enter_comment' => 'Введите коментарий',
    'assessment' => 'Оценка',
    'add_comment' => 'Добавит коментарий',
    'delete' => 'Удалить',
    'edit' => 'Изменить',
    'update_photo' => 'Обновить фото',
    'photo_title' => 'Заголовок',
    'chose_picture' => 'Выбирите фото',
    'add_photo' => 'Добивть фото',
    'update_comment' => 'Обновить коментарий',
    'my_page' => 'Моя станица',
];
