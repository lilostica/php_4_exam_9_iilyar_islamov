@extends('layouts.app')
@section('content')
    <h1 style="margin-top: 50px" class="main-title">@lang('messages.all_photo')</h1>
        <div class="row">
            @foreach($photos as $photo)
            <div style="margin-top: 20px" class="col col-md-4">
                <div class="card" style="width: 18rem;">
                    @if($photo->photo !== 'noPhoto.jpg')
                        <img class="card-img-top" src="{{asset('/storage/' .
                        $photo->photo)}}" alt="Photo">
                    @else
                        <img src="{{asset('noPhoto.jpg')}}" class="card-img-top" alt="No photo">
                    @endif
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{route('photo.show',['photo' => $photo])}}">{{$photo->title}}</a></h5>
                        <h5>@lang('messages.author'): <a href="{{route('user-show',$photo->user->id)}}">{{$photo->user->name}}</a></h5>
                        <h6>@lang('messages.avg'): {{number_format($photo->comments->avg('assessment'),2)}}</h6>
                    </div>
                </div>
            </div>
                @endforeach
        </div>
    <div class="row p-5">
        <div class="col-12 offset-4">
            {{ $photos->links() }}
        </div>
    </div>
    @endsection
