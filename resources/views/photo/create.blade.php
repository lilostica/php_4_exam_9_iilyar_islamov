@extends('layouts.app')
@section('content')
    <h1 style="margin-top: 25px;">@lang('messages.add_photo')</h1>
    <form method="post" action="{{route('photo.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">@lang('messages.photo_title')</label>
            <input type="text"  class="form-control @error('title') is-invalid @enderror" id="title" name="title">
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="customFile" name="photo">
                @error('photo')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <label class="custom-file-label" for="customFile">@lang('messages.chose_picture')</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">@lang('messages.add_photo')</button>
    </form>
    @endsection
