@extends('layouts.app')
@section('content')
    <h1>{{$photo->title}}</h1>
    @if($photo->photo !== 'noPhoto.jpg')
        <img class="card-img-top" src="{{asset('/storage/' .
                        $photo->photo)}}" alt="Photo">
    @else
        <img src="{{asset('noPhoto.jpg')}}" class="card-img-top" alt="No photo">
    @endif
    <h4>@lang('messages.avg'): {{number_format($photo->comments->avg('assessment'),2)}}</h4>
    <button style="margin-top: 10px;" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#comment{{$photo->id}}" aria-expanded="false" aria-controls="collapseExample">
        @lang('messages.comments')
    </button>
    @foreach($photo->comments as $comment)
        <div class="collapse comment" id="comment{{$photo->id}}">
            <div style="margin-top: 10px" class="card card-body">
                <h4>{{$comment->user->name}}</h4>
                {{$comment->body}}
                <p>@lang('messages.assessment'): {{$comment->assessment}}</p>
                @can('delete',$comment)
                <form  method="post"  action="{{ route('comment.destroy',$comment->id) }}">
                    @method('DELETE')
                    @csrf
                    <button class="like btn-danger" type="submit">@lang('messages.delete')</button>
                </form>
                @endcan
                @can('edit',$comment)
                <a style="width: 100px; margin-top: 5px" class="btn btn-primary" href="{{route('comment.edit',$comment->id)}}">@lang('messages.edit')</a>
                    @endcan
            </div>
        </div>
    @endforeach
    <form method="post" action="{{route('comment.store')}}">
        @csrf
        <input type="hidden" name="photo_id" value="{{$photo->id}}">
        <div class="form-group">
            <label for="comment">@lang('messages.enter_comment')</label>
            <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="comment" rows="3"></textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="assessment">@lang('messages.assessment')</label>
            <select name="assessment" id="assessment" class="custom-select  @error('assessment') is-invalid @enderror">
                <option selected value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            @error('assessment')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button style="margin-bottom: 20px;" class="btn-primary" type="submit">@lang('messages.add_comment')</button>
    </form>
    @endsection
