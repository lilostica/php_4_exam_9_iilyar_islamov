@extends('layouts.app')
@section('content')
    <h1 style="margin-top: 20px;">@lang('messages.update_comment')</h1>
    <form method="post" action="{{route('comment.update',['comment' => $comment])}}">
        @method('put')
        @csrf
        <input type="hidden" name="photo_id" value="{{$comment->photo->id}}">
        <div class="form-group">
            <label for="comment">@lang('messages.enter_comment')</label>
            <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="comment" rows="3">{{$comment->body}}</textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="assessment">@lang('messages.assessment')</label>
            <select name="assessment" id="assessment" class="custom-select  @error('assessment') is-invalid @enderror">
                <option @if($comment->assessment === "1.00") selected @endif value="1">1</option>
                <option @if($comment->assessment === "2.00") selected @endif value="2">2</option>
                <option @if($comment->assessment === "3.00") selected @endif value="3">3</option>
                <option @if($comment->assessment === "4.00") selected @endif value="4">4</option>
                <option @if($comment->assessment === "5.00") selected @endif value="5">5</option>
            </select>
            @error('assessment')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button class="btn-primary" type="submit">@lang('messages.update_comment')</button>
    </form>
    @endsection
