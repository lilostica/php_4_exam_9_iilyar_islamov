@extends('layouts.app')
@section('content')
    <h1 style="margin-top: 20px">{{$user->name}}</h1>
    @can('displayCreateButton',$user)
    <button type="button" class="btn btn-outline-success">
        <a class="links" href="{{route('photo.create')}}">@lang('messages.add_photo')</a>
    </button>
    @endcan
    <div class="row">
    @foreach($user->photos as $photo)
        <div style="margin-top: 20px" class="col col-md-4">
            <div class="card" style="width: 18rem;">
                @if($photo->photo !== 'noPhoto.jpg')
                    <img class="card-img-top" src="{{asset('/storage/' .
                        $photo->photo)}}" alt="Photo">
                @else
                    <img src="{{asset('noPhoto.jpg')}}" class="card-img-top" alt="No photo">
                @endif
                <div class="card-body">
                    <h5 class="card-title"><a href="{{route('photo.show',['photo' => $photo])}}">{{$photo->title}}</a></h5>
                    <h5>@lang('messages.author'): {{$photo->user->name}}</h5>
                    <h6>@lang('messages.avg'): {{number_format($photo->comments->avg('assessment'),2)}}</h6>
                    <div class="row">
                        @can('edit',$photo)
                    <button  style="margin-right: 10px" type="button" class="btn btn-outline-success">
                        <a href="{{route('photo.edit',['photo' => $photo->id])}}">@lang('messages.edit')</a>
                    </button>
                        @endcan
                        @can('delete',$photo)
                    <form action="{{route('photo.destroy',['photo' => $photo->id])}}" method="post">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger">@lang('messages.delete')</button>
                    </form>
                            @endcan
                </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
    @endsection
