<?php

namespace Tests\Feature;

use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @photo
     */
    public function testLoginStorePhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $this->actingAs($user);
        $response = $this->post(route('photo.store',$photo));
        $response->assertStatus(302);
    }
    /**
     * @photo
     */
    public function testUnLoginStorePhoto()
    {
        $photo = [
            'user_id' => 1,
            'title' => 'title',
            'photo' => 'test_photo.jpg'
        ];
        $response = $this->post(route('photo.store',$photo));
        $response->assertRedirect(route('login'));
    }

    /**
     * @photo
     */
    public function testLoginCreatePhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $this->actingAs($user);
        $response = $this->get(route('photo.edit',$photo->id));
        $response->assertStatus(200);
    }

    /**
     * @photo
     */
    public function testUnLoginCreatePhoto()
    {
        $response = $this->get(route('photo.edit',1));
        $response->assertRedirect(route('login'));
    }

    /**
     * @photo
     */
    public function testUnLoginShowPhoto()
    {
        $response = $this->get(route('photo.show',1));
        $response->assertRedirect(route('login'));
    }

    /**
     * @photo
     */
    public function testLoginShowPhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $this->actingAs($user);
        $response = $this->get(route('photo.show',$photo->id));
        $response->assertStatus(200);
    }

    /**
     * @photo
     */
    public function testUnLoginIndex()
    {
        $response = $this->get('/photo');
        $response->assertStatus(200);
    }
    /**
     * @photo
     */
    public function testLoginUpdatePhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $update_data = [
            'user_id' => $user->id,
            'title' => 'title updated',
            'photo' => 'test.jpg'
        ];
        $this->actingAs($user);
        $response = $this->put(route('photo.update',$photo,$update_data));
        $response->assertStatus(302);
    }

    /**
     * @photo
     */
    public function testUnLoginUpdatePhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $update_data = [
            'user_id' => $user->id,
            'title' => 'title updated',
            'photo' => 'test.jpg'
        ];
        $response = $this->put(route('photo.update',$photo,$update_data));
        $response->assertRedirect(route('login'));
    }

    /**
     * @photo
     */
    public function testLoginDestroyPhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $this->actingAs($user);
        $response = $this->delete(route('photo.destroy',$photo,$photo->id));
        $response->assertStatus(302);
    }

    /**
     * @photo
     */
    public function testUnLoginDestroyPhoto()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $response = $this->delete(route('photo.destroy',$photo,$photo->id));
        $response->assertRedirect(route('login'));
    }
}
