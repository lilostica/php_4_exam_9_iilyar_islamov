<?php

namespace Tests\Feature;

use App\Comment;
use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
  use RefreshDatabase;


    /**
     * @comment
     */
    public function testLoginStore()
   {
       $user = factory(User::class)->create();
       $photo =factory(Photo::class)->create([
           'user_id' => $user->id,
           'title' => 'title',
           'photo' => 'test.jpg'
       ]);
       $comment = factory(Comment::class)
           ->create([
               'user_id' => $user->id,
               'photo_id' => $photo->id
           ]);
       $this->actingAs($user);
       $response = $this->post(route('photo.store',$comment));
       $response->assertStatus(302);
   }

    public function testUnLoginStore()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $user->id,
                'photo_id' => $photo->id
            ]);
        $response = $this->post(route('photo.store',$comment));
        $response->assertRedirect(route('login'));
    }

    /**
     * @comment
     */
    public function testLoginEdit()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $user->id,
                'photo_id' => $photo->id
            ]);
        $this->actingAs($user);
        $response = $this->get(route('photo.edit',$comment));
        $response->assertStatus(200);
    }

    /**
     * @comment
     */
    public function testUnLoginEdit()
    {
        $user = factory(User::class)->create();
        $photo =factory(Photo::class)->create([
            'user_id' => $user->id,
            'title' => 'title',
            'photo' => 'test.jpg'
        ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $user->id,
                'photo_id' => $photo->id
            ]);
        $response = $this->get(route('photo.edit',$comment));
        $response->assertRedirect(route('login'));
    }
}
