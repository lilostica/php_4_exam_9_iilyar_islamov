<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'user_id' => rand(1,10),
        'photo_id' => rand(1,60),
        'body' => $faker->text,
        'assessment' => rand(1,5)
    ];
});
